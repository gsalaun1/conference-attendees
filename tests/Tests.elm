module Tests exposing (packTest, packsTest)

import Expect exposing (Expectation)
import Main exposing (Attendee, Pack, attendeeInPack, attendeesInPack)
import Test exposing (..)


packTest : Test
packTest =
    describe "attendeeInPack"
        [ test "should return true if contains attendee " <|
            \_ ->
                let
                    attendee : Attendee
                    attendee =
                        { name = "attendee"
                        , days = [ "DAY1" ]
                        }

                    pack : Pack
                    pack =
                        { name = "pack"
                        , days = [ "DAY1" ]
                        }
                in
                Expect.equal (attendeeInPack pack attendee) True
        , test "should return false if does not contain attendee " <|
            \_ ->
                let
                    attendee : Attendee
                    attendee =
                        { name = "attendee"
                        , days = [ "DAY2" ]
                        }

                    pack : Pack
                    pack =
                        { name = "pack"
                        , days = [ "DAY1" ]
                        }
                in
                Expect.equal (attendeeInPack pack attendee) False
        ]


packsTest : Test
packsTest =
    describe "attendeesInPack"
        [ test "should return empty if no attendees" <|
            \_ ->
                let
                    pack : Pack
                    pack =
                        { name = "pack"
                        , days = [ "DAY1" ]
                        }
                in
                Expect.equal (attendeesInPack [] pack) []
        , test "should return matching attendees" <|
            \_ ->
                let
                    attendee1 : Attendee
                    attendee1 =
                        { name = "attendee"
                        , days = [ "DAY1" ]
                        }

                    attendee2 : Attendee
                    attendee2 =
                        { name = "attendee"
                        , days = [ "DAY2" ]
                        }

                    pack : Pack
                    pack =
                        { name = "pack"
                        , days = [ "DAY1" ]
                        }
                in
                Expect.equal (attendeesInPack [ attendee1, attendee2 ] pack) [ attendee1 ]
        ]
