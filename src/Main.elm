--module Main exposing (Model, Msg(..), init, main, update, view)


module Main exposing (Attendee, Pack, attendeeInPack, attendeesInPack)

import Browser
import Html exposing (Html, div, span, table, td, text, th, thead, tr)
import Html.Attributes exposing (class)
import Http exposing (Error(..))
import Json.Decode as Decode exposing (Decoder, field, list, string)
import String



-- MAIN


main =
    Browser.element { init = init, view = view, update = update, subscriptions = subscriptions }



-- MODEL


type alias Model =
    { conferences : List Conference
    , selectedConference : Maybe Conference
    , status : Status
    }


type Status
    = Failure String
    | Loading
    | Success


type alias DayId =
    String


type alias ConferenceDay =
    { id : String
    , day : DayId
    , date : String
    }


type alias Conference =
    { name : String
    , start : String
    , end : String
    , days : List ConferenceDay
    , packs : List Pack
    , attendees : List Attendee
    }


type alias Pack =
    { name : String
    , days : List DayId
    }


type alias Attendee =
    { name : String
    , days : List DayId
    }


init :
    ()
    -> ( Model, Cmd Msg )
init _ =
    ( { conferences = [], selectedConference = Nothing, status = Loading }
    , loadConferences
    )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none



-- UPDATE


type Msg
    = GotConferences (Result Http.Error (List Conference))


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GotConferences result ->
            case result of
                Ok value ->
                    ( { model | conferences = value, selectedConference = List.head value, status = Success }, Cmd.none )

                Err error ->
                    case error of
                        BadUrl message ->
                            ( { model | status = Failure message }, Cmd.none )

                        BadStatus status ->
                            ( { model | status = Failure <| String.fromInt status }, Cmd.none )

                        BadBody message ->
                            ( { model | status = Failure message }, Cmd.none )

                        _ ->
                            ( { model | status = Failure "Erreur réseau" }, Cmd.none )



-- VIEW


view : Model -> Html Msg
view model =
    case model.status of
        Failure message ->
            span [ class "title" ] [ text <| "Impossible de charger les données : " ++ message ]

        Loading ->
            span [ class "title" ] [ text "Données en cours de chargement" ]

        Success ->
            case model.selectedConference of
                Just conference ->
                    div []
                        [ div [ class "header" ]
                            [ div [ class "title" ] [ text <| "Participants à la conférence " ++ conference.name ]
                            ]
                        , div [ class "tickets" ]
                            (List.map (displayPack conference.attendees) conference.packs
                                ++ List.map displayTickets conference.days
                            )
                        ]

                Nothing ->
                    div [ class "header" ]
                        [ span [ class "title" ] [ text "Aucune conférence sélectionnée" ]
                        ]


displayPack : List Attendee -> Pack -> Html Msg
displayPack attendees pack =
    div []
        [ span [] [ text pack.name ]
        , table
            []
            ([ thead []
                [ th [] [ text "Participant" ]
                , th [] [ text "Jours" ]
                ]
             ]
                ++ List.map displayAttendee (attendeesInPack attendees pack)
            )
        ]


displayAttendee : Attendee -> Html Msg
displayAttendee attendee =
    tr []
        [ td [] [ text attendee.name ]
        , td [] [ text <| displayDays attendee.days ]
        ]


displayDays : List DayId -> String
displayDays daysId =
    String.concat daysId


displayTickets : ConferenceDay -> Html Msg
displayTickets conferenceDay =
    div [] [ span [] [ text conferenceDay.date ] ]


loadConferences : Cmd Msg
loadConferences =
    Http.get
        { url = "./conferences.json"
        , expect = Http.expectJson GotConferences conferencesDecoder
        }


conferencesDecoder : Decoder (List Conference)
conferencesDecoder =
    field "conferences" (list conferenceDecoder)


conferenceDecoder : Decoder Conference
conferenceDecoder =
    Decode.map6 Conference
        (field "name" string)
        (field "start" string)
        (field "end" string)
        (field "days" conferenceDaysDecoder)
        (field "packs" packsDecoder)
        (field "attendees" attendeesDecoder)


conferenceDaysDecoder : Decoder (List ConferenceDay)
conferenceDaysDecoder =
    Decode.list conferenceDayDecoder


conferenceDayDecoder : Decoder ConferenceDay
conferenceDayDecoder =
    Decode.map3 ConferenceDay
        (field "id" string)
        (field "day" dayIdDecoder)
        (field "date" string)


packsDecoder : Decoder (List Pack)
packsDecoder =
    Decode.list packDecoder


packDecoder : Decoder Pack
packDecoder =
    Decode.map2 Pack
        (field "name" string)
        (field "days" <| Decode.list dayIdDecoder)


attendeesDecoder : Decoder (List Attendee)
attendeesDecoder =
    Decode.list attendeeDecoder


attendeeDecoder : Decoder Attendee
attendeeDecoder =
    Decode.map2 Attendee
        (field "name" string)
        (field "days" <| Decode.list dayIdDecoder)


dayIdDecoder : Decoder DayId
dayIdDecoder =
    Decode.string


attendeesInPack : List Attendee -> Pack -> List Attendee
attendeesInPack attendees pack =
    List.filter (attendeeInPack pack) attendees


attendeeInPack : Pack -> Attendee -> Bool
attendeeInPack pack attendee =
    pack.days == attendee.days
